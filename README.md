# ASP.NET / ASP.NET Core WebAPI AngularJS / Angular Demo with Component based design, Webpack, Ahead-of-Time (Aot) Compilation, Lazy Loading and Treeshaking runnable on every Platform (Cross Platform)

Forked from: https://github.com/FabianGosebrink/ASPNET-ASPNETCore-Angular-Webpack

This repository offers you one demo application implemented three times (AngularJS, Angular with SystemJS and Angular with Webpack) with two compatible endpoints implemented in ASP.NET & ASP.NET Core served to run on every platform (Cross Platform)

### In this case we will use AngularJS without Webpack and other stuff. For more details relate to original project.

## AngularJS Client (main dir)

This client is implemented with component based design and the one-way dataflow. It is using gulp as a taskrunner to minify and uglify the javascript files. By running 

```npm install```

and 

```npm start```

the application starts and runs in your default browser.

By typing 

```npm run buildProd```

the application build in a ".dist"-folder and you can then type 

```npm run liteProd```

to serve the files from the ".dist"-folder.
